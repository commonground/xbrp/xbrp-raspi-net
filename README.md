This Ansible playbook turns a fleet of Raspberry Pi into Kubernetes cattle with k3s, the streamlined version of Kubernetes by @rancher that runs silky smooth on the ARM processor powering your Pi.

# Manually prepare your Raspberries
- Burn Raspian Stretch Lite on an SD card with EtcherBalena, or something alike
- Create an empty `ssh` file in the root of the SD card (volume is called `boot`)

# Secure your Raspberries
Optional but advisable: use SSH key auth and disable password login. 
The default ssh credentials for a Raspberry Pi are username `pi` and password `raspberry`.
- Copy your pubkey to the Pi with `ssh-copy-id pi@kube1.example.com`
- In `/etc/ssh/sshd_config` on the Pi, set `PasswordAuthentication` to `no`

# Configure ansible setup
- Copy `ansible/hosts.template` to `ansible/hosts` for your configuration
- Choose one of the Raspberries to lead / orchestrate your cluster; we'll call this the _server_
- In `ansible/hosts`, fill out the ip or hostname for the leading Raspberry under `[k3s-server]`
- Fill out the ip's or hostnames for the rest of your cattle under `[k3s-agents]`

I personally prefer using the Pi's hardware mac address to assign a hostname and ip address within the LAN by DHCP, but you could also set a static ip address on the Pi.

# Provision the nodes
When you're all set up and configured, run the Ansible playbook:
```bash
$ make
```

This will:
- Install the k3s binary on the 'server' Pi (the leading node)
- Install a `k3s-server` service on the server and start it
- Fetch the node token from the server
- Install and start a `k3s-agent` service on the agents, joining the cluster
- Enable autostart on boot for k3s on all nodes

# See if it worked
Log into your server node and run:
```bash
$ sudo k3s kubectl get node -o wide
```
You should see all of your nodes broadcasting a _Ready_ status.

Happy cattle herding!

# Network configuration

192.168.178.101 desq-raspi-net-master ("master")
192.168.178.17 groenestede
192.168.178.94 bovenhove
192.168.178.95 rni
192.168.178.129 oudescha
192.168.178.150 heidedal

# Steps to configure desq-raspi-net-master ("Master")

- Install Raspbian full
- Enable ssh (by putting ssh file in sd root)
- Set password for user pi to something secure
- Set hostname to `desq-raspi-net-master`
- Install kubectl (check version (32 or 64bit) with `uname -m`, armv7l=32bit)
- For 32bit: `curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/arm/kubectl && chmod +x ./kubectl && sudo mv ./kubectl /usr/local/bin/kubectl && kubectl version --client --short"`
- Install k9s: `sudo apt update && sudo apt install snapd`, reboot (`sudo reboot`) and then `sudo snap install core && sudo snap install k9s && alias k9s="k9s --kubeconfig ~/.kube/config"`

# Steps to configure a PI Cluster for a zone <zoneid>

On the pi for the zone:
- Install Raspian Lite
- Enable ssh (by putting ssh file in sd root)
- Set password for user pi to <zoneid> `raspi-config`
- Set hostname to <zoneid> `raspi-config`

On the master run:
- Copy public ssh key of master `ssh-copy-id pi@<ip address>` (run this on master)
- Add hosts-<zoneid> configuration file `ansible/hosts-<zoneid>` containing the ip of the zone pi
- Install k3s: `ansible-playbook ./ansible/site.yml -i ./ansible/hosts-<zoneid>`

On the pi for the zone run:
- `sudo cat /etc/rancher/k3s/k3s.yaml`
- Merge the output of this command into ~/.kube/config on the desq-raspi-net-master
- Rename/reconfigure the names of the resources to avoid conflicts
- Set the kubectl context to <zoneid> `kubectl config use-context <zoneid>`
- Use kubectl or k9s to see workloads `kubectl get pods --all-namespaces` or `k9s`
